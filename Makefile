subdirs=n1

.PHONY: $(subdirs)

all: $(subdirs)

$(subdirs):
	make -C $@
